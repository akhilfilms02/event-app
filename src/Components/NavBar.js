import React, { Component } from "react";
import Auth from "./Auth";
import fire from "./Firebase";
import { Link } from "react-router-dom";
import Logo from "../assets/logo.png";
import {
  MDBNavbar,
  MDBBtn,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavItem,
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem,
  MDBIcon,
  MDBNavLink,
  MDBHamburgerToggler,
  MDBCollapse
} from "mdbreact";

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.notifyRef = fire
      .firestore()
      .collection("users")
      .doc(localStorage.getItem("userId"))
      .collection("notification");
    this.state = {
      notifications: []
    };
  }

  state = {
    collapse1: false,
    collapseID: ""
  };

  toggleCollapse = collapseID => () => {
    this.setState(prevState => ({
      collapseID: prevState.collapseID !== collapseID ? collapseID : ""
    }));
  };

  toggleSingleCollapse = collapseId => {
    this.setState({
      ...this.state,
      [collapseId]: !this.state[collapseId]
    });
  };
  notifyCollection = querySnapshot => {
    const notifications = [];
    querySnapshot.forEach(doc => {
      const { name } = doc.data();
      notifications.push({
        key: doc.id,
        name
      });
    });
    this.setState({
      notifications
    });
    // console.log(this.state.notifications);
  };
  deleteNotification(notify) {
    this.setState({
      notifications: this.state.notifications.filter(data => {
        return data.name !== notify.name;
      })
    });
    fire
      .firestore()
      .collection("users")
      .doc(localStorage.getItem("userId"))
      .collection("notification")
      .doc(notify.key)
      .delete();
  }
  componentDidMount() {
    this.notifyRef.onSnapshot(this.notifyCollection);
  }
  logout() {
    localStorage.removeItem("userId");
    Auth.authenticated = false;
    window.location.reload();
  }
  render() {
    return (
      <MDBNavbar
        className="navbar-shadows"
        style={{ position: "fixed", width: "100%", top: 0, zIndex: 9 }}
        color="white"
        dark
        expand="md"
      >
        <MDBNavbarBrand className=" d-flex flex-row align-items-center">
          <a href="/eventslist">
            <img
              width="200"
              className="img-fluid py-2"
              src={Logo}
              alt="SNAPPENINGS"
            />
          </a>
        </MDBNavbarBrand>

        <MDBNavbarNav right>
          <MDBNavItem>
            <MDBDropdown>
              <MDBDropdownToggle nav caret>
                <MDBIcon className="p-2 dropdown notif" icon="bell">
                  <span className="badge badge-danger ml-2">
                    {this.state.notifications.length === 0
                      ? null
                      : this.state.notifications.length}
                  </span>
                </MDBIcon>
              </MDBDropdownToggle>
              <MDBDropdownMenu className="dropdown-default">
                {this.state.notifications.length === 0 ? (
                  <MDBDropdownItem>
                    {" "}
                    <div style={{ fontSize: "12px" }}>
                      No New Notifications
                    </div>{" "}
                  </MDBDropdownItem>
                ) : (
                  this.state.notifications.map((notify, i) => (
                    <MDBDropdownItem key={i}>
                      <div style={{ fontSize: "12px" }}>
                        Sorry! The Event {notify.name} was removed{" "}
                        <MDBBtn
                          href="#"
                          onClick={() => this.deleteNotification(notify)}
                        >
                          <i
                            className="fas fa-times "
                            style={{ color: "orange" }}
                          />
                        </MDBBtn>
                      </div>
                    </MDBDropdownItem>
                  ))
                )}
              </MDBDropdownMenu>
            </MDBDropdown>
          </MDBNavItem>
        </MDBNavbarNav>
        <MDBHamburgerToggler
          className="d-flex d-md-none "
          color="#FF512F"
          id="hamburger1"
          onClick={() => this.toggleSingleCollapse("collapse1")}
        />
        {/* <i
          color="#FF512F"
          id="hamburger1"
          onClick={() => this.toggleSingleCollapse("collapse1")}
          className="d-flex d-md-none hamburger fa fa-bars"
        /> */}
        <MDBCollapse
          className="dropdown"
          id="navbarCollapse3"
          isOpen={this.state.collapse1}
          navbar
        >
          <MDBNavbarNav right>
            <MDBNavItem>
              <MDBDropdown>
                <MDBDropdownToggle nav caret>
                  <span
                    className="dropdown raleway"
                    style={{ color: "#e43228" }}
                  >
                    EVENTS <i className="fas fa-caret-down" />
                  </span>
                </MDBDropdownToggle>
                <MDBDropdownMenu className="dropdown-default">
                  <MDBDropdownItem>
                    <Link
                      style={{
                        color: "#e43228",
                        fontSize: "12px",
                        cursor: "pointer",
                        fontWeight: "bold",
                        fontFamily: "Raleway"
                      }}
                      to="/participated"
                    >
                      Participated Events
                    </Link>
                  </MDBDropdownItem>
                  <MDBDropdownItem>
                    <Link
                      style={{
                        color: "#e43228",
                        fontSize: "12px",
                        cursor: "pointer",
                        fontWeight: "bold",
                        fontFamily: "raleway"
                      }}
                      to="/created"
                    >
                      Created Events
                    </Link>
                  </MDBDropdownItem>
                </MDBDropdownMenu>
              </MDBDropdown>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink
                className="dropdown raleway"
                to="#"
                onClick={() => this.logout()}
              >
                LOGOUT
              </MDBNavLink>
            </MDBNavItem>
          </MDBNavbarNav>
        </MDBCollapse>
      </MDBNavbar>
    );
  }
}

export default NavBar;
