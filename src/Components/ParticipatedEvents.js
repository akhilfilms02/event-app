import React, { Component } from "react";
import fire from "./Firebase";
import { Link } from "react-router-dom";
import Event from "./Event";
import NavBar from "./NavBar";
export class ParticipatedEvents extends Component {
  constructor(props) {
    super(props);
    this.ref = fire
      .firestore()
      .collection("users")
      .doc(localStorage.getItem("userId"))
      .collection("participated events");
    this.state = {
      modal: false,
      data: [],
      loader: true
    };
  }
  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  };
  onCollectionUpdate = querySnapshot => {
    const data = [];
    querySnapshot.forEach(doc => {
      const {
        eventName,
        desc,
        duration,
        fees,
        location,
        maxParticipants,
        date
      } = doc.data();
      data.push({
        id: doc.id,
        eventName,
        desc,
        duration,
        fees,
        location,
        maxParticipants,
        date
      });
      this.setState({
        data,
        loader: false
      });
    });
  };
  componentDidMount() {
    this.ref.onSnapshot(this.onCollectionUpdate);
  }
  render() {
    return (
      <div className="main">
        <NavBar />
        {this.state.loader === true && this.state.data.length > 0 ? (
          <div className="loader small" />
        ) : null}
        <div className="spacer" />
        <Link to="/eventslist">
          <i
            style={{ position: "absolute", paddingLeft: "20px" }}
            className=" text-dark  text-left mt-5 fas fa-arrow-left"
          />
        </Link>
        <h1
          className="mt-5 text-black text-center p-0"
          style={{ fontWeight: "bold" }}
        >
          Participated Events
        </h1>
        <div className="spacer" />
        <div className="event-box mx-auto d-flex flex-column">
          {this.state.data.length > 0 ? (
            this.state.data.map((event, i) => (
              <div key={i}>
                <Event
                  id={event.id}
                  key={event.key}
                  eventName={event.eventName}
                  desc={event.desc}
                  duration={event.duration}
                  location={event.location}
                  fees={event.fees}
                  tags={event.tags}
                  maxParticipants={event.maxParticipants}
                  createdBy={event.createdBy}
                  date={event.date}
                />
              </div>
            ))
          ) : (
            <h5 className="mt-5 text-black text-center p-0">
              -You Have Not Partcipated Any Events-
            </h5>
          )}
        </div>
      </div>
    );
  }
}

export default ParticipatedEvents;
