import React, { Component } from "react";
import fire from "./Firebase";
import NavBar from "./NavBar";
import { Formik, Form, Field } from "formik";
import { Link } from "react-router-dom";
import * as Yup from "yup";
import Notification from "./Notification";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {
  MDBModal,
  MDBBtn,
  MDBModalBody,
  MDBModalHeader,
  MDBModalFooter,
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
  MDBCol
} from "mdbreact";

export class CreateEvent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      eventName: "",
      desc: "",
      duration: "",
      location: "",
      maxParticipants: "",
      fees: "",
      createdBy: localStorage.getItem("userId"),
      requirements: ["Name", " Mobile Number", "Organization Name", "Address"],
      requirement: "",
      modal: false,
      color: null,
      date: null,
      message: null
    };
  }
  SignupSchema = Yup.object().shape({
    eventName: Yup.string()
      .min(4, "Too Short!")
      .max(30, "Too Long!")
      .required("Required"),
    desc: Yup.string()
      .min(10, "Too Short!")
      .max(300, "Too Long!")
      .required("Required!"),
    location: Yup.string()
      .min(2, "Too Short")
      .max(150, "Too Long")
      .required("Required!"),
    duration: Yup.string()
      .min(3, "Too Short!")
      .max(10, "Too Long!")
      .required("Required!"),
    fees: Yup.number()
      .min(0, "Too Short!")
      .required("Required!"),
    maxParticipants: Yup.number()
      .min(5, "Too Short!")
      .required("Required!")
  });
  submitHandler(val) {
    const db = fire.firestore();
    this.setState(() => ({
      eventName: val.eventName,
      desc: val.desc,
      duration: val.duration,
      location: val.location,
      maxParticipants: val.maxParticipants,
      fees: val.fees
    }));
    // console.log(this.state)
    db.collection("events")
      .add(this.state)
      .then(() => {
        this.setState({
          color: "success",
          message: "Event Has Been Created Successfully"
        });
        setTimeout(() => {
          this.props.history.push("/eventslist");
        }, 1000);
      })
      .catch(error => {
        this.setState({
          color: "danger",
          message: error.message
        });
      });
  }
  addReq() {
    this.setState({
      requirements: [...this.state.requirements, this.state.requirement],
      requirement: ""
    });
  }
  deleteReq(data) {
    this.setState({
      requirements: this.state.requirements.filter(hello => {
        return data !== hello;
      })
    });
  }
  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  };
  render() {
    return (
      <div className="main">
        <NavBar />
        <div className="spacer" />
        <div className="spacer" />
        <div className="spacer" />
        <Notification color={this.state.color} message={this.state.message} />
        <MDBCol className="d-flex justify-content-center">
          <MDBCard style={{ width: "70%" }}>
            <MDBCardBody>
              <MDBCardTitle>
                <div className="d-flex flex-row align-items-center">
                  <Link
                    to="/eventslist"
                    className=" text-dark text-left fas fa-arrow-left"
                  />
                  <span style={{ paddingLeft: "22px" }}>Create Event</span>
                </div>
              </MDBCardTitle>
              <Formik
                initialValues={{
                  eventName: "",
                  desc: "",
                  duration: "",
                  location: "",
                  maxParticipants: "",
                  fees: ""
                }}
                validationSchema={this.SignupSchema}
                onSubmit={values => {
                  this.submitHandler(values);
                }}
              >
                {({ errors, touched }) => (
                  <Form className="form-group p-3">
                    <MDBCardText>
                      <label htmlFor="eventName" className="black-text">
                        Enter the Event Name &nbsp;
                      </label>
                      {errors.eventName && touched.eventName ? (
                        <span class="align-items-end badge badge-warning">
                          <div>{errors.eventName}</div>
                        </span>
                      ) : null}
                      <Field
                        type="text"
                        name="eventName"
                        placeholder="Event Name"
                        className="form-control form-control-md my-2"
                      />
                      <label htmlFor="desc" className="black-text">
                        Enter the Event Description &nbsp;
                      </label>
                      {errors.desc && touched.desc ? (
                        <span class="align-items-end badge badge-warning">
                          <div>{errors.desc}</div>
                        </span>
                      ) : null}
                      <Field
                        type="text"
                        name="desc"
                        placeholder="Description"
                        className="form-control form-control-md my-2"
                      />
                      <label htmlFor="duration" className="black-text">
                        Enter the Event Duration {"( Ex :  5 Hrs )"} &nbsp;
                      </label>
                      {errors.duration && touched.duration ? (
                        <span class="align-items-end badge badge-warning">
                          <div>{errors.duration}</div>
                        </span>
                      ) : null}
                      <Field
                        type="text"
                        name="duration"
                        placeholder="Duration"
                        className="form-control form-control-md my-2"
                      />
                      <label htmlFor="location" className="black-text">
                        Enter the Event Location &nbsp;
                      </label>
                      {errors.location && touched.location ? (
                        <span class="align-items-end badge badge-warning">
                          <div>{errors.location}</div>
                        </span>
                      ) : null}
                      <Field
                        type="text"
                        name="location"
                        placeholder="Location"
                        className="form-control form-control-md my-2"
                      />
                      <label htmlFor="maxParticipants" className="black-text">
                        Enter the Maximum No Of Guests &nbsp;
                      </label>
                      {errors.maxParticipants && touched.maxParticipants ? (
                        <span class="align-items-end badge badge-warning">
                          <div>{errors.maxParticipants}</div>
                        </span>
                      ) : null}
                      <Field
                        type="text"
                        name="maxParticipants"
                        placeholder="Max Participants"
                        className="form-control form-control-md my-2"
                      />
                      <label htmlFor="fees" className="black-text">
                        Enter the Event Fees &nbsp;
                      </label>
                      {errors.fees && touched.fees ? (
                        <span class="align-items-end badge badge-warning">
                          <div>{errors.fees}</div>
                        </span>
                      ) : null}
                      <Field
                        type="text"
                        name="fees"
                        placeholder="Fees"
                        className="form-control form-control-md my-2"
                      />
                      <div className="datePickerFlex">
                        {" "}
                        <label htmlFor="eveDesc" className="black-text">
                          Select a Date and Time &nbsp;
                        </label>
                        <DatePicker
                          name="date"
                          className="datePickerInput"
                          placeholderText="select a date/time"
                          selected={this.state.date}
                          onChange={e => this.setState({ date: e })}
                          showTimeSelect
                          timeFormat="hh:mm"
                          timeIntervals={15}
                          dateFormat="MMMM d, yyyy h:mm aa"
                          timeCaption="time"
                        />
                      </div>
                    </MDBCardText>
                    <div className="d-flex justify-content-left">
                      <MDBBtn className="btn btn-white" onClick={this.toggle}>
                        {this.state.requirements.length > 0
                          ? "Added Requirements"
                          : "Add Requirements"}
                      </MDBBtn>
                      <button
                        className="btn btn-secondary py-3"
                        type="submit"
                        disabled={
                          this.state.requirements.length === 0 ||
                          this.state.date == null
                        }
                      >
                        Create
                      </button>
                    </div>
                    <MDBModal
                      position="top"
                      isOpen={this.state.modal}
                      toggle={this.toggle}
                      className="pt-5"
                    >
                      <MDBModalHeader toggle={this.toggle}>
                        Add Any Requirements
                      </MDBModalHeader>
                      <MDBModalBody>
                        <button
                          className="btn btn-sm btn-secondary "
                          onClick={() => this.addReq()}
                          disabled={this.state.requirement === ""}
                          style={{ marginBottom: "20px" }}
                        >
                          <span>Add</span>&nbsp;
                          <i className="fa fa-plus" />
                        </button>
                        <br />
                        <input
                          placeholder="Enter Requirement"
                          className="form-control"
                          type="text"
                          value={this.state.requirement}
                          onChange={e =>
                            this.setState({ requirement: e.target.value })
                          }
                        />

                        {this.state.requirements.map((data, i) => {
                          return (
                            <span
                              className="list-group-item-modal  d-flex flex-row align-items-center justify-content-between"
                              key={i}
                            >
                              {data}{" "}
                              <button
                                className="btn btn-danger btn-sm mr-0"
                                onClick={() => this.deleteReq(data)}
                              >
                                <i className="fa fa-trash" />
                              </button>
                            </span>
                          );
                        })}
                      </MDBModalBody>

                      <MDBModalFooter>
                        <button
                          className="btn btn-white btn-sm"
                          onClick={this.toggle}
                        >
                          Close
                        </button>
                        <button
                          className="btn btn-secondary btn-sm"
                          onClick={this.toggle}
                          disabled={this.state.requirements.length === 0}
                        >
                          OK
                        </button>
                      </MDBModalFooter>
                    </MDBModal>
                  </Form>
                )}
              </Formik>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
        <div className="spacer" />
      </div>
    );
  }
}

export default CreateEvent;
