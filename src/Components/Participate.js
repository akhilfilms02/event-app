import React, { Component } from "react";
import fire from "./Firebase";
import NavBar from "./NavBar";
import { Link } from "react-router-dom";
import Notification from "./Notification";
import {
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
  MDBCol
} from "mdbreact";

export class Participate extends Component {
  constructor(props) {
    super(props);
    this.stringify = null;
    this.state = {
      requirements: [],
      values: [],
      value: "",
      color: null,
      message: null
    };
  }
  componentWillMount() {
    const json = this.props.match.params.data.replace(":", "");

    this.stringify = JSON.parse(json);
    this.setState({
      requirements: [...this.stringify.requirements]
    });
  }
  submitHandler() {
    const db = fire.firestore();
    db.collection("events")
      .doc(this.stringify.docId)
      .collection("participants")
      .doc(localStorage.getItem("userId"))
      .set({
        key: this.state.requirements,
        values: this.state.values
      });

    db.collection("users")
      .doc(localStorage.getItem("userId"))
      .collection("participated events")
      .doc(this.stringify.docId)
      .set(this.stringify)
      .then(() => {
        this.setState({
          color: "success",
          message: "You Are Participating In This Event"
        });
        setTimeout(() => {
          this.props.history.push("/eventslist");
        }, 1000);
      })
      .catch(error => {
        this.setState({
          color: "danger",
          message: error.message
        });
      });
    // console.log(this.state.values);
  }
  createUI() {
    return this.state.requirements.map((el, i) => (
      <div key={i} className="">
        <label className="black-text"> {el}</label>
        <input
          className="form-control my-2"
          type="text"
          placeholder={el}
          onChange={this.handleChange.bind(this, i)}
        />
      </div>
    ));
  }
  handleChange(i, event) {
    let values = [...this.state.values];

    values[i] = event.target.value;
    this.setState({ values, value: event.target.value });
  }
  render() {
    return (
      <div>
        <Notification color={this.state.color} message={this.state.message} />
        <div className="spacer" />
        <div className="spacer" />
        <div className="spacer" />
        <MDBCol className="d-flex justify-content-center">
          <MDBCard style={{ width: "80%" }}>
            <MDBCardBody>
              <MDBCardTitle>
                <div className="d-flex flex-row align-items-center">
                  <Link
                    to="/eventslist"
                    className=" text-dark text-left fas fa-arrow-left"
                  />
                  <span style={{ paddingLeft: "22px" }}>
                    Enter Your Details
                  </span>
                </div>
              </MDBCardTitle>
              <MDBCardText>
                <div className="form-group p-3">
                  {this.createUI()}
                  <div className="d-flex justify-content-left" />
                </div>
              </MDBCardText>
              <button
                className="btn btn-success px-4 py-3"
                onClick={() => this.submitHandler()}
                disabled={
                  this.state.values.length === 0 ||
                  this.state.value === "" ||
                  this.state.values.length !== this.state.requirements.length
                }
              >
                Confirm
              </button>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
        <NavBar />
      </div>
    );
  }
}

export default Participate;
