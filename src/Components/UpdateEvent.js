import React, { Component } from "react";
import fire from "./Firebase";
import NavBar from "./NavBar";
import * as Yup from "yup";
import { Link } from "react-router-dom";
import Notification from "./Notification";
import Footer from "./Footer";
import {
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
  MDBCol
} from "mdbreact";

import { Formik, Form, Field } from "formik";

export class UpdateEvent extends Component {
  constructor() {
    super();
    this.value = this.state = {
      id: "",
      eventName: "",
      desc: "",
      duration: "",
      location: "",
      maxParticipants: "",
      fees: "",
      color: null,
      message: null
    };
  }
  componentWillMount() {
    // console.log(JSON.parse(this.props.match.params.id));
    const {
      id,
      eventName,
      desc,
      duration,
      location,
      maxParticipants,
      fees
    } = JSON.parse(this.props.match.params.id);
    this.setState({
      id,
      eventName,
      desc,
      duration,
      location,
      maxParticipants,
      fees
    });
  }
  SignupSchema = Yup.object().shape({
    eventName: Yup.string()
      .min(4, "Too Short!")
      .max(30, "Too Long!")
      .required("Required"),
    desc: Yup.string()
      .min(10, "Too Short!")
      .max(300, "Too Long!")
      .required("Required!"),
    location: Yup.string()
      .min(2, "Too Short")
      .max(150, "Too Long")
      .required("Required!"),
    duration: Yup.string()
      .min(3, "Too Short!")
      .max(10, "Too Long!")
      .required("Required!"),
    fees: Yup.number()
      .min(0, "Too Short!")
      .required("Required!"),
    maxParticipants: Yup.number()
      .min(5, "Too Short!")
      .required("Required!")
  });
  onSubmitHandler(key, val) {
    const db = fire.firestore();
    this.setState(() => ({
      eventName: val.eventName,
      desc: val.desc,
      duration: val.duration,
      location: val.location,
      maxParticipants: val.maxParticipants,
      fees: val.fees
    }));
    db.collection("events")
      .doc(key)
      .update(this.state)
      .then(() => {
        this.setState({
          color: "success",
          message: "Event Has Been Updated Successfully"
        });
        setTimeout(() => {
          this.props.history.push("/eventslist");
        }, 1000);
      })
      .catch(error => {
        this.setState({
          color: "danger",
          message: error.message
        });
      });
  }
  render() {
    return (
      <div className="main">
        <NavBar />
        <div className="spacer" />
        <div className="spacer" />
        <Notification color={this.state.color} message={this.state.message} />
        <MDBCol className="d-flex justify-content-center">
          <MDBCard style={{ width: "70%" }}>
            <MDBCardBody>
              <MDBCardTitle>
                <div className="d-flex flex-row align-items-center">
                  <Link
                    to="/eventslist"
                    className=" text-dark text-left fas fa-arrow-left"
                  />
                  <span style={{ paddingLeft: "22px" }}>Update Event</span>
                </div>
              </MDBCardTitle>
              <Formik
                initialValues={{
                  eventName: this.state.eventName,
                  desc: this.state.desc,
                  duration: this.state.duration,
                  location: this.state.location,
                  maxParticipants: this.state.maxParticipants,
                  fees: this.state.fees
                }}
                validationSchema={this.SignupSchema}
                onSubmit={values => {
                  this.onSubmitHandler(this.state.id, values);
                }}
              >
                {({ errors, touched }) => (
                  <Form className="form-group p-3">
                    <MDBCardText>
                      <label htmlFor="eveName" className="black-text">
                        Enter the Event Name &nbsp;
                      </label>
                      {errors.eventName && touched.eventName ? (
                        <span class="align-items-end badge badge-warning">
                          <div>{errors.eventName}</div>
                        </span>
                      ) : null}
                      <Field
                        id="eveName"
                        placeholder="Event Name"
                        name="eventName"
                        className="form-control form-control-md my-2"
                      />
                      <label htmlFor="eveDesc" className="black-text">
                        Enter the Event Description &nbsp;
                      </label>
                      {errors.desc && touched.desc ? (
                        <span class="align-items-end badge badge-warning">
                          <div>{errors.desc}</div>
                        </span>
                      ) : null}
                      <Field
                        name="desc"
                        placeholder="Describe your Event"
                        className="form-control form-control-md my-2"
                        // value={this.state.desc}
                        // onChange={e => {
                        //   this.setState({
                        //     desc: e.target.value
                        //   });
                        // }}
                      />
                      <label htmlFor="eveDesc" className="black-text">
                        Enter the Event Duration {"( Ex :  5 Hrs )"} &nbsp;
                      </label>
                      {errors.duration && touched.duration ? (
                        <span class="align-items-end badge badge-warning">
                          <div>{errors.duration}</div>
                        </span>
                      ) : null}
                      <Field
                        name="duration"
                        placeholder="Duration"
                        className="form-control form-control-md my-2"
                        // value={this.state.duration}
                        // onChange={e => {
                        //   this.setState({
                        //     duration: e.target.value
                        //   });
                        // }}
                      />
                      <label htmlFor="eveDesc" className="black-text">
                        Enter the Event Location &nbsp;
                      </label>
                      {errors.location && touched.location ? (
                        <span class="align-items-end badge badge-warning">
                          <div>{errors.location}</div>
                        </span>
                      ) : null}
                      <Field
                        name="location"
                        placeholder="Location"
                        className="form-control form-control-md my-2"
                        // value={this.state.location}
                        // onChange={e => {
                        //   this.setState({
                        //     location: e.target.value
                        //   });
                        // }}
                      />
                      <label htmlFor="eveDesc" className="black-text">
                        Enter the Maximum No Of Guests &nbsp;
                      </label>
                      {errors.maxParticipants && touched.maxParticipants ? (
                        <span class="align-items-end badge badge-warning">
                          <div>{errors.maxParticipants}</div>
                        </span>
                      ) : null}
                      <Field
                        name="maxParticipants"
                        placeholder="No of Participants"
                        className="form-control form-control-md my-2"
                        // value={this.state.maxParticipants}
                        // onChange={e => {
                        //   this.setState({
                        //     maxParticipants: e.target.value
                        //   });
                        // }}
                      />
                      <label htmlFor="eveDesc" className="black-text">
                        Enter the Event Fees &nbsp;
                      </label>
                      {errors.fees && touched.fees ? (
                        <span class="align-items-end badge badge-warning">
                          <div>{errors.fees}</div>
                        </span>
                      ) : null}
                      <Field
                        name="fees"
                        placeholder="Fees"
                        className="form-control form-control-md my-2"
                        // value={this.state.fees}
                        // onChange={e => {
                        //   this.setState({
                        //     fees: e.target.value
                        //   });
                        // }}
                      />
                    </MDBCardText>
                    <button className="btn btn-success" type="submit">
                      Submit
                    </button>
                  </Form>
                )}
              </Formik>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
        <div className="spacer" />
        <Footer />
      </div>
    );
  }
}

export default UpdateEvent;
