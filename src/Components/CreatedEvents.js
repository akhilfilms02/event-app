import React, { Component } from "react";
import fire from "./Firebase";
import { Link } from "react-router-dom";
import Event from "./Event";
import NavBar from "./NavBar";
export class CreatedEvents extends Component {
  constructor(props) {
    super(props);
    this.ref = fire.firestore().collection("events");
    this.state = {
      modal: false,
      data: [],
      loader: true
    };
  }
  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  };
  onCollectionUpdate = querySnapshot => {
    const data = [];
    querySnapshot.forEach(doc => {
      if (localStorage.getItem("userId") === doc.data().createdBy) {
        const {
          eventName,
          desc,
          duration,
          fees,
          location,
          maxParticipants,
          createdBy,
          date
        } = doc.data();

        data.push({
          id: doc.id,
          eventName,
          desc,
          duration,
          fees,
          location,
          maxParticipants,
          createdBy,
          date
        });
        this.setState({
          data,
          loader: false
        });
      }
    });
  };
  componentDidMount() {
    this.ref.orderBy("date", "asc").onSnapshot(this.onCollectionUpdate);
  }
  render() {
    return (
      <div className="main">
        <NavBar />
        {this.state.loader === true ? <div className="loader small" /> : null}
        <div className="spacer" />
        <Link to="/eventslist">
          <i
            style={{ position: "absolute", paddingLeft: "20px" }}
            className=" text-dark title text-left mt-5 fas fa-arrow-left"
          />
        </Link>
        <h2 className="display-5 mt-3 pt-5 raleway text-black text-center">
          EVENTS CREATED BY YOU
        </h2>
        <div className="spacer" />
        <div className="event-box mx-auto d-flex flex-column">
          <div className="list-index d-flex justify-content-between text-white text-center">
            <span>Event Name</span>
            <span className="pr-4">Event Status</span>
          </div>
          {this.state.data.map((event, i) => (
            <div key={i}>
              <Event
                id={event.createdBy}
                key={event.key}
                eventName={event.eventName}
                desc={event.desc}
                duration={event.duration}
                location={event.location}
                fees={event.fees}
                tags={event.tags}
                maxParticipants={event.maxParticipants}
                createdBy={event.createdBy}
                date={event.date}
              />
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default CreatedEvents;
