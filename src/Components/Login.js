import React, { Component } from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import fire from "./Firebase";
import Auth from "./Auth";
import Notification from "./Notification";
import Logo from "../assets/Snappenings.png";
import tagline from "../assets/logo.png";

class Login extends Component {
  constructor() {
    super();
    this.state = {
      color: null,
      message: null
    };
  }
  SignupSchema = Yup.object().shape({
    email: Yup.string()
      .email()
      .required("Required"),
    password: Yup.string()
      .min(6)
      .max(16)
      .required("Required")
  });

  login = (email, password) => {
    fire
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(user => {
        Auth.authenticated = true;
        localStorage.setItem("userId", user.user.uid);
        this.setState({
          color: "success",
          message: "Login Successfull"
        });
        setTimeout(() => {
          this.props.history.push("/eventslist");
        }, 1000);
        console.log(user);
      })
      .catch(error => {
        this.setState({
          color: "danger",
          message: error.message
        });
      });
  };
  render() {
    return (
      <div className="main">
        <div className="loginContainer">
          <nav className="navbar py-1 bg-light">
            <p className="mx-auto py-0 my-1 navbar-brand">
              <img
                src={Logo}
                width="40"
                height="40"
                alt=""
                className="img-fluid"
              />
            </p>
          </nav>
          <Notification color={this.state.color} message={this.state.message} />
          <div className="wrapper w-1">
            <div className="my-2">
              <p className="text-center raleway my-1">LOGIN TO</p>
              <img
                className="d-block mx-auto"
                width="250"
                src={tagline}
                alt="Sign-In"
              />
            </div>
            <Formik
              initialValues={{
                email: "",
                password: ""
              }}
              validationSchema={this.SignupSchema}
              onSubmit={values => {
                this.login(values.email, values.password);
              }}
            >
              {({ errors, touched }) => (
                <Form>
                  <div className="input-group pt-4">
                    <label htmlFor="email" className="label-text">
                      E-mail : &nbsp;
                    </label>
                    {errors.email && touched.email ? (
                      <span className="p-2 badge badge-warning">
                        <div>{errors.email}</div>
                      </span>
                    ) : null}
                    <Field
                      className="form-control input-box my-2 w-100"
                      placeholder="Username"
                      name="email"
                      type="email"
                    />
                  </div>
                  <div className="input-group pt-4">
                    <label htmlFor="password" className="label-text">
                      Password : &nbsp;
                    </label>
                    {errors.password && touched.password ? (
                      <span className="p-2 badge badge-warning">
                        <div>{errors.password}</div>
                      </span>
                    ) : null}
                    <Field
                      className="form-control input-box my-2 w-100"
                      placeholder="Password"
                      name="password"
                      type="password"
                    />
                  </div>
                  <div className="d-flex pt-3 justify-content-around">
                    <button className="btn btn-info" type="submit">
                      Login
                    </button>
                    <a className="btn btn-primary" href="/signup">
                      Sign Up
                    </a>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
