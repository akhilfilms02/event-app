import React, { Component } from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import fire from "./Firebase";
import Notification from "./Notification";
import Logo from "../assets/Snappenings.png";
import tagline from "../assets/logo.png";

class Register extends Component {
  constructor() {
    super();
    this.state = {
      color: null,
      message: null
    };
  }
  SignupSchema = Yup.object().shape({
    email: Yup.string()
      .email()
      .required("Required"),
    username: Yup.string()
      .min(6)
      .max(16)
      .required("Required"),
    password: Yup.string()
      .min(6)
      .max(16)
      .required("Required")
  });

  signup = (email, username, password) => {
    const db = fire.firestore();
    fire
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(user => {
        db.collection("users").add({
          id: user.user.uid,
          username: username,
          email: user.user.email
        });
        this.setState({
          color: "success",
          message: "Signed Up Succesfully"
        });
        setTimeout(() => {
          this.props.history.push("/login");
        }, 1000);
      })
      .catch(error => {
        this.setState({
          color: "danger",
          message: "Sign Up Failed"
        });
      });
  };
  render() {
    return (
      <div className="main">
        <div className="loginContainer">
          <nav className="navbar py-1 bg-light">
            <p className="mx-auto py-0 my-1 navbar-brand">
              <img
                src={Logo}
                width="40"
                height="40"
                alt=""
                className="img-fluid"
              />
            </p>
          </nav>
          <Notification color={this.state.color} message={this.state.message} />
          <div className="wrapper w-2">
            <div className="my-2">
              <p className="text-center raleway my-1">REGISTER ON</p>
              <img
                className="d-block mx-auto"
                width="250"
                src={tagline}
                alt="Sign-In"
              />
            </div>
            <Formik
              initialValues={{
                email: "",
                username: "",
                password: ""
              }}
              validationSchema={this.SignupSchema}
              onSubmit={(values, action) => {
                this.signup(values.email, values.username, values.password);
              }}
            >
              {({ errors, touched }) => (
                <Form>
                  <div className="input-group pt-4 ">
                    <label htmlFor="email" className="label-text">
                      E-mail : &nbsp;
                    </label>
                    {errors.email && touched.email ? (
                      <span className="p-2 badge badge-warning">
                        <div>{errors.email}</div>
                      </span>
                    ) : null}
                    <Field
                      className="form-control w-100"
                      placeholder="Email"
                      name="email"
                      type="email"
                    />
                  </div>

                  <div className="input-group pt-4 ">
                    <label htmlFor="email" className="label-text">
                      Username : &nbsp;
                    </label>
                    {errors.username && touched.username ? (
                      <span className="p-2 badge badge-warning">
                        <div>{errors.username}</div>
                      </span>
                    ) : null}
                    <Field
                      className="form-control w-100"
                      placeholder="Username"
                      name="username"
                      type="text"
                    />
                  </div>

                  <div className="input-group pt-4 pb-1 ">
                    <label htmlFor="email" className="label-text">
                      Password : &nbsp;
                    </label>
                    {errors.email && touched.email ? (
                      <span className="p-2 badge badge-warning">
                        <div>{errors.email}</div>
                      </span>
                    ) : null}
                    <Field
                      className="form-control w-100"
                      placeholder="Password"
                      name="password"
                      type="password"
                    />
                  </div>

                  <div className="d-flex justify-content-around pt-3">
                    <button className="btn btn-info" type="submit">
                      Sign Up
                    </button>
                    <a className="btn btn-primary" href="/">
                      Login
                    </a>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    );
  }
}

export default Register;
