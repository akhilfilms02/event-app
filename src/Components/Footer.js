import React, { Component } from "react";
import { MDBFooter } from "mdbreact";

class Footer extends Component {
  render() {
    return (
      <div className="footer mt-2 p-0">
        <MDBFooter style={{ height: "100%" }} className="font-small pt-3 mt-0">
          <p className="text-center">
            Made with{" "}
            <span role="img" aria-label="emoji">
              ❤️
            </span>{" "}
            by Saraf and Akhil
          </p>
        </MDBFooter>
      </div>
    );
  }
}

export default Footer;
