import React, { Component } from "react";
import Auth from "./Auth";
import Event from "./Event";
import fire from "./Firebase";
import NavBar from "./NavBar";
import Footer from "./Footer";
// import { MDBSpinner } from "mdbreact";
import {
  Link,
  Element,
  Events,
  animateScroll as scroll,
  scroller
} from "react-scroll";

import { MDBIcon } from "mdbreact";
import Notification from "./Notification";

export class EventsList extends Component {
  constructor(props) {
    super(props);
    this.scrollToTop = this.scrollToTop.bind(this);
    this.db = fire.firestore();
    this.ref = fire
      .firestore()
      .collection("events")
      .orderBy("date", "asc");
    this.notifyRef = fire
      .firestore()
      .collection("users")
      .doc(localStorage.getItem("userId"))
      .collection("notification");

    this.state = {
      loader: true,
      events: [],
      notifications: [],
      color: null,
      message: null
    };
  }

  logout() {
    localStorage.removeItem("userId");
    Auth.authenticated = false;
    this.props.history.push("/");
  }
  onCollectionUpdate = querySnapshot => {
    const events = [];
    querySnapshot.forEach(doc => {
      const {
        eventName,
        desc,
        duration,
        location,
        fees,
        maxParticipants,
        createdBy,
        date
      } = doc.data();
      events.push({
        key: doc.id,
        doc,
        eventName,
        desc,
        duration,
        location,
        fees,
        maxParticipants,
        createdBy,
        date
      });
    });
    this.setState({
      events
    });
  };

  notifyCollection = querySnapshot => {
    const notifications = [];
    querySnapshot.forEach(doc => {
      const { name } = doc.data();
      notifications.push({
        key: doc.id,
        name
      });
    });
    this.setState({
      notifications,
      loader: false
    });
    // console.log(this.state.notifications);
  };

  componentDidMount() {
    this.ref.onSnapshot(this.onCollectionUpdate);
    this.notifyRef.onSnapshot(this.notifyCollection);
    Events.scrollEvent.register("begin", function() {
      console.log("begin", arguments);
    });
    Events.scrollEvent.register("end", function() {
      console.log("end", arguments);
    });
  }
  scrollToTop() {
    scroll.scrollToTop();
  }
  scrollTo() {
    scroller.scrollTo("scroll-to-element", {
      duration: 800,
      delay: 0,
      smooth: "easeInOutQuart"
    });
  }

  componentWillUnmount() {
    Events.scrollEvent.remove("begin");
    Events.scrollEvent.remove("end");
  }
  deleteEvent(key, eventName) {
    this.setState({
      events: this.state.events.filter(data => {
        return data.key !== key;
      })
    });
    const db = fire.firestore();
    db.collection("events")
      .doc(key)
      .collection("participants")
      .onSnapshot(data => {
        data.forEach(doc => {
          if (doc.exists) {
            db.collection("users")
              .doc(doc.id)
              .collection("notification")
              .add({
                name: eventName
              });
          }
        });
      });
    db.collection("events")
      .doc(key)
      .delete()
      .then(() => {
        this.setState({
          color: "success",
          message: "Event Has Been Updated Successfully"
        });
      })
      .catch(error => {
        this.setState({
          color: "danger",
          message: error.message
        });
      });
  }
  deleteNotification(notify) {
    this.setState({
      notifications: this.state.notifications.filter(data => {
        return data.name !== notify.name;
      })
    });
    this.db
      .collection("users")
      .doc(localStorage.getItem("userId"))
      .collection("notification")
      .doc(notify.key)
      .delete();
  }
  participate(key) {
    fire
      .firestore()
      .collection("events")
      .doc(key)
      .get()
      .then(doc => {
        const data = { ...doc.data(), docId: doc.id };
        this.props.history.push(`/participate/:${JSON.stringify(data)}`);
      });
  }
  render() {
    return (
      <div className="main d-flex flex-column align">
        <NavBar />
        <div className="spacer" />
        <div className="spacer" />
        <section className="section-bg ">
          <div className="spacer" />
          <div className="spacer" />
          <div className="spacer" />
          <div className="spacer" />
          {this.state.loader === true ? <div className="loader small" /> : null}

          <h1 className="display-2 text-white text-center">All Your Events</h1>
          <h2 className="display-4 text-white text-center">
            Managed in Single App
          </h2>
          <div className="d-flex justify-content-center">
            <div className="d-flex justify-content-start mt-5 pb-3">
              <button
                className=" btn btn-sm btn-danger p-3 justify-content-between  mb-2"
                style={{
                  fontFamily: "'Poppins', sans-serif !important",
                  fontWeight: "bold"
                }}
                onClick={() => this.props.history.push("/createevent")}
              >
                Create An Event <MDBIcon className="pl-2" icon="plus" />
              </button>
            </div>
            <div className="d-flex justify-content-start mt-5 pb-3">
              <Link
                activeClass="active"
                className="test1 btn btn-sm btn-light p-3 justify-content-between  mb-2"
                to="test1"
                spy={true}
                smooth={true}
                duration={500}
                style={{
                  fontFamily: "'Poppins', sans-serif !important",
                  fontWeight: "bold"
                }}
              >
                Browse Events{" "}
                <MDBIcon className="pl-2" icon="angle-double-down" />
              </Link>
            </div>
          </div>
        </section>
        {this.state.loader === true ? <div className="loader small" /> : null}
        <Notification color={this.state.color} message={this.state.message} />
        <Element name="test1" className="element" />
        <div className="spacer" />
        <div className="spacer" />
        <div>
          <h2 className="display-5 pb-5 raleway text-black text-center">
            EVENTS HOSTED ON SNAPPENINGS
          </h2>
          <div className="list-index d-flex justify-content-between align-items-center text-white text-center">
            <span className="eventNameHeader">Event</span>
            <span className="eventStatus">Event Status</span>
          </div>
          {this.state.events.map(event => (
            <Event
              key={event.key}
              id={event.doc.id}
              eventName={event.eventName}
              desc={event.desc}
              duration={event.duration}
              location={event.location}
              fees={event.fees}
              tags={event.tags}
              maxParticipants={event.maxParticipants}
              createdBy={event.createdBy}
              requirements={event.requirements}
              date={event.date}
              delete={this.deleteEvent.bind(
                this,
                event.doc.id,
                event.eventName
              )}
              participate={this.participate.bind(this, event.doc.id)}
            />
          ))}
        </div>
        <div className="spacer" />
        <div className="spacer" />
        <Footer />
      </div>
    );
  }
}

export default EventsList;
