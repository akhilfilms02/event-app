import React, { Component } from "react";
import { Link } from "react-router-dom";
import fire from "./Firebase";
import Moment from "react-moment";
import {
  MDBModal,
  MDBBtn,
  MDBModalBody,
  MDBModalFooter,
  MDBModalHeader
} from "mdbreact";
export class Event extends Component {
  constructor(props) {
    super();
    this.state = {
      modal: false,
      id: props.id,
      eventName: props.eventName,
      desc: props.desc,
      duration: props.duration,
      location: props.location,
      fees: props.fees,
      maxParticipants: props.maxParticipants,
      createdBy: props.createdBy,
      requirements: props.requirements,
      participated: [],
      date: props.date.seconds * 1000
    };
  }
  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  };

  componentDidMount() {
    fire
      .firestore()
      .collection("users")
      .doc(localStorage.getItem("userId"))
      .collection("participated events")
      .onSnapshot(docs => {
        docs.forEach(doc => {
          if (doc.data().docId === this.state.id) {
            // console.log(doc.data().docId);
            this.setState({
              participated: doc.data().docId
            });
          }
        });
      });
  }
  render() {
    return (
      <div>
        <ul className="list-group">
          <li className="list-group-item doom d-flex flex-row flex-start align-items-center">
            {
              <div className="date dContainer">
                <Moment
                  format="D"
                  className="eventDate eventDay"
                  withTitle
                  date={this.state.date}
                />
                <Moment
                  format="MMM"
                  className="eventDate eventMonth"
                  withTitle
                  date={this.state.date}
                />
              </div>
            }

            <div onClick={this.toggle} className="eventName">
              <strong
                className="eventName "
                style={{
                  // textTransform: "uppercase",
                  // fontFamily: "'Poppins', sans-serif",
                  fontWeight: "bold",
                  fontSize: "15px",
                  cursor: "pointer",
                  textAlign: "left"
                }}
              >
                {this.state.eventName}
              </strong>
            </div>
            <div className="button">
              {this.state.createdBy === localStorage.getItem("userId") ? (
                <Link
                  className="btn btn-sm participants-btn "
                  to={`/participants/${this.state.id}`}
                >
                  Participants
                </Link>
              ) : null}

              {this.state.createdBy !== localStorage.getItem("userId") &&
              this.state.participated !== this.props.id ? (
                <button
                  className="btn btn-sm participate-btn"
                  onClick={this.props.participate}
                >
                  Participate
                </button>
              ) : null}

              {this.state.participated === this.props.id ? (
                <button className="btn  btn-sm  participate-btn2" disabled>
                  Participated
                </button>
              ) : null}
            </div>
          </li>
        </ul>

        <MDBModal
          isOpen={this.state.modal}
          toggle={this.toggle}
          fullHeight
          position="bottom"
        >
          <MDBModalHeader toggle={this.toggle}>
            <span
              ref={title => (this.title = title)}
              style={{ textTransform: "uppercase" }}
            >
              {this.state.eventName}
            </span>{" "}
            &nbsp;
            {this.state.createdBy === localStorage.getItem("userId") ? (
              <>
                <Link
                  className="btn btn-success btn-sm"
                  to={`/updateevent/${JSON.stringify(this.state)}`}
                >
                  Edit
                </Link>
                <button
                  className="btn btn-danger btn-sm"
                  onClick={this.props.delete}
                >
                  Delete
                </button>{" "}
              </>
            ) : null}
          </MDBModalHeader>
          <MDBModalBody>
            <p>
              <strong style={{ fontWeight: "bold" }}>Description :</strong>
              {"  "}
              {this.state.desc}
            </p>
            <p>
              <strong style={{ fontWeight: "bold" }}>Duration :</strong>
              {"  "}
              {this.state.duration}
            </p>
            <p>
              <strong style={{ fontWeight: "bold" }}>Location :</strong>
              {"  "}
              {this.state.location}
            </p>
            <p>
              <strong style={{ fontWeight: "bold" }}>Fees :</strong>
              {"  "}
              {this.state.fees}
            </p>
            <p>
              <strong style={{ fontWeight: "bold" }}>Max Members :</strong>
              {"  "}
              {this.state.maxParticipants}
            </p>
          </MDBModalBody>
          <MDBModalFooter>
            <MDBBtn color="danger" onClick={this.toggle}>
              Close
            </MDBBtn>
          </MDBModalFooter>
        </MDBModal>
      </div>
    );
  }
}

export default Event;
