import React, { Component } from "react";
import fire from "./Firebase";
import { Link } from "react-router-dom";
import NavBar from "./NavBar";
export class Participants extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);
    this.ref = fire
      .firestore()
      .collection("events")
      .doc(this.props.match.params.id);

    this.state = {
      participantsKey: [],
      participantsValue: [],
      docData: []
    };
  }
  componentDidMount() {
    this.ref.collection("participants").onSnapshot(data => {
      data.forEach(doc => {
        this.state.docData.push(doc.data());
        this.setState({
          participantsKey: [...doc.data().key],
          participantsValue: [...doc.data().values]
        });
      });
    });
  }
  render() {
    return (
      <div>
        <NavBar />
        <div className="spacer" />
        <div className="spacer" />

        <Link to="/eventslist">
          <i
            style={{ position: "absolute", paddingLeft: "20px" }}
            className=" text-dark text-left mt-5 fas fa-arrow-left"
          />
        </Link>
        <h1
          className="mt-5 text-black text-center p-0"
          style={{ fontWeight: "bold" }}
        >
          Participants
        </h1>
        <p
          className="text-center p-0"
          style={{ fontStyle: "italic", color: "#666" }}
        >
          Total Participants: {this.state.docData.length}
        </p>
        <div className="spacer" />
        {this.state.docData.length > 0 ? (
          this.state.docData.map((data, i) => (
            <div className="mb-4">
              <div className="list-index d-flex justify-content-between align-items-center text-white text-center">
                <span className="eventNameHeader">Participant No {i + 1}</span>
              </div>
              <div
                key={i}
                className="participantsContainer "
                style={{ paddingTop: "20px" }}
              >
                {data.key.map((doc, j) => (
                  <li key={j} className="participantsKey ">
                    <b>{doc}</b>&nbsp;:
                    <span className="participantsValue">{data.values[j]}</span>
                  </li>
                ))}
              </div>
            </div>
          ))
        ) : (
          <h5 className="mt-5 text-black text-center p-0">
            -No Participants Yet-
          </h5>
        )}
      </div>
    );
  }
}

export default Participants;
