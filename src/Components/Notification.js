import React, { Component } from "react";

import { MDBContainer, MDBAlert } from "mdbreact";

export class Notification extends Component {
  render() {
    return (
      <>
        {this.props.color === "success" ? (
          <MDBContainer className="main-alert">
            <MDBAlert className="alert-success" color={this.props.color}>
              <i className="fa fa-check" /> &nbsp;
              {this.props.message}
            </MDBAlert>
          </MDBContainer>
        ) : null}
        {this.props.color === "danger" ? (
          <MDBContainer className="main-alert">
            <MDBAlert className="alert-failure" color={this.props.color}>
              <i className="fa fa-exclamation-triangle" /> &nbsp;
              {this.props.message}
            </MDBAlert>
          </MDBContainer>
        ) : null}
      </>
    );
  }
}
export default Notification;
